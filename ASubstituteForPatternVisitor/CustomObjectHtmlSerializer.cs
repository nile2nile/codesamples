﻿//   (c) by Vadim V. Riabinin, 2019
//  mailto: nile2nile@gmail.com
// BTC: 19KLDifi2DkEjjwwe3q63yUJkUJatsAMCQ

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ASubstituteForPatternVisitor.Namespace1;
using ASubstituteForPatternVisitor.Namespace2;

namespace ASubstituteForPatternVisitor
{
    public class CustomObjectHtmlSerializer : CustomObjectOperationBase
    {
        protected override void ProcessCustomType1(object customObjectInstance)
        {
            CustomType1 concreteTypedInstance = (CustomType1)customObjectInstance;

            Console.WriteLine(nameof(CustomObjectHtmlSerializer) + " for " + concreteTypedInstance.GetType().FullName);
        }

        protected override void ProcessCustomType2(object customObjectInstance)
        {
            CustomType2 concreteTypedInstance = (CustomType2)customObjectInstance;

            Console.WriteLine(nameof(CustomObjectHtmlSerializer) + " for " + concreteTypedInstance.GetType().FullName);
        }
    }
}
