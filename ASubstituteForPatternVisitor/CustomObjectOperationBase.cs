﻿//   (c) by Vadim V. Riabinin, 2019
//  mailto: nile2nile@gmail.com
// BTC: 19KLDifi2DkEjjwwe3q63yUJkUJatsAMCQ

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ASubstituteForPatternVisitor.Namespace1;
using ASubstituteForPatternVisitor.Namespace2;

namespace ASubstituteForPatternVisitor
{
    public abstract class CustomObjectOperationBase
    {
        private readonly Dictionary<string, Action<object>> m_operationMethodMap = new Dictionary<string, Action<object>>();

        protected CustomObjectOperationBase()
        {
            // it seems enough to use type full name (with a namespace) as a key here; at least for example
            m_operationMethodMap.Add(typeof(CustomType1).FullName, ProcessCustomType1);
            m_operationMethodMap.Add(typeof(CustomType2).FullName, ProcessCustomType2);
        }

        public void RunOperation(object customObjectInstance)
        {
            var operationMethodKey = customObjectInstance.GetType().FullName;
            if (!m_operationMethodMap.ContainsKey(operationMethodKey))
                throw new InvalidOperationException("Custom object doesn't support current operation");

            m_operationMethodMap[operationMethodKey](customObjectInstance);
        }

        protected virtual void ProcessCustomType1(object customObjectInstance) { }
        protected virtual void ProcessCustomType2(object customObjectInstance) { }
    }
}
