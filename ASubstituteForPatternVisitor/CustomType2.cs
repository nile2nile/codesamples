﻿//   (c) by Vadim V. Riabinin, 2019
//  mailto: nile2nile@gmail.com
// BTC: 19KLDifi2DkEjjwwe3q63yUJkUJatsAMCQ

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ASubstituteForPatternVisitor.Namespace2
{
    class CustomType2
    {
        public string Param3 { get; set; }
        public string Param4 { get; set; }
        public string Param5 { get; set; }
    }
}
