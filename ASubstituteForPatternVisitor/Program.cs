﻿//   (c) by Vadim V. Riabinin, 2019
//  mailto: nile2nile@gmail.com
// BTC: 19KLDifi2DkEjjwwe3q63yUJkUJatsAMCQ

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ASubstituteForPatternVisitor.Namespace1;
using ASubstituteForPatternVisitor.Namespace2;

// // Custom Objects Operationist pattern replacement/fix for the classic GoF behavioral pattern "Visitor".
// // Pattern Visitor definition seems to be a GoF one:
// // https://metanit.com/sharp/patterns/3.11.php

// // // No changes made to entity classes.
// // // No need to make a public interface with methods that process a variety of entities (see classic Visitor pattern realization). 
// // // No Double Dispatching.
// // // No ObjectStructure class of Add-Remove-Accept methods. Use a custom objects list, mapped via the CustomObjectOperationBase ctor. 
// // // No code that explicitly exploits System.Reflection namespace functionality.

namespace ASubstituteForPatternVisitor
{
    class Program
    {
        static void Main(string[] args)
        {
            var customObjectList = new List<object>();
            customObjectList.Add(new CustomType1());
            customObjectList.Add(new CustomType2());

            var customObjectOperationist1 = new CustomObjectXmlSerializer();
            var customObjectOperationist2 = new CustomObjectHtmlSerializer();
            customObjectList.ForEach(coi => customObjectOperationist1.RunOperation(coi));
            customObjectList.ForEach(coi => customObjectOperationist2.RunOperation(coi));

            Console.ReadKey();
        }
    }
}
